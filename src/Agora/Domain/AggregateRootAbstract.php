<?php
namespace Agora\Domain;

/**
 * 抽象集約ルート
 */
class AggregateRootAbstract
{

    /** Traits */
    use
        // 集約ルートバージョントレイト
        AggregateRootVersionTrait;

}
