<?php
namespace Agora\Domain;

/**
 * プロパティアクセスサポート抽象ドメインモデル
 */
class PropertyAccessDomainModelAbstract
{

    /** @var array プロパティリスト {<name: string>: <getter: string|null>, ...} */
    protected const PROPERTIES = [];

    /**
     * 未定義フィールドへの参照サポート
     * @param string $name フィールド名
     * @return mixed
     * @throws \LogicException アクセス不能
     */
    public function __get(string $name)
    {
        $className = static::class;
        $exceptionMessage = "undefined field "
            . "{$className}::{$name} for defined properties";

        // 対応するgetterがあれば実行して値を取得
        if (!is_null($getter = $this->getFieldGetter($className, $name))) {
            if (!method_exists($this, $getter)) {
                throw new \LogicException($exceptionMessage);
            }
            return $this->{$getter}();
        }

        // フィールドから値を取得
        if (!property_exists($this, $name)) {
            throw new \LogicException($exceptionMessage);
        }

        return $this->{$name};
    }

    /**
     * 未定義フィールドへのisset(), empty() サポート
     * @param string $name フィールド名
     * @return mixed
     */
    public function __isset(string $name): bool
    {
        try {
            $this->__get($name);
            return true;
        } catch (\LogicException $e) {
            return false;
        }
    }

    /**
     * フィールドに対応したgetterメソッドを取得
     * @param string $className クラス完全修飾名
     * @param string $fieldName フィールド名
     * @return string|null
     * @throws \LogicException 公開フィールド未定義
     */
    private function getFieldGetter(
        string $className, string $fieldName): ?string
    {
        $parentClassName = get_parent_class($className);
        if ($parentClassName === false) {
            // 基底クラスが存在しない(当該クラス)であれば例外
            $className = static::class;
            throw new \LogicException(
                "undefined field {$className}::{$fieldName}");
        }

        // フィールド名が定義されていれば対応するgetter名を返却
        if (array_key_exists($fieldName, $className::PROPERTIES)) {
            return $className::PROPERTIES[$fieldName];
        }

        // 親クラスの定義を検索
        return $this->getFieldGetter($parentClassName, $fieldName);
    }

}
