<?php
namespace Agora\Domain;

/**
 * ドメイン通知ハンドラインターフェース
 */
interface INotificationHandler
{

    /**
     * 通知する
     * @param Notification $notification ドメイン通知
     * @return INotificationHandler
     */
    public function notify(Notification $notification): INotificationHandler;

}
