<?php
namespace Agora\Domain;

/**
 * ドメイン通知種別インターフェース
 */
interface INotificationTypes
{

    /** @var int 不明 */
    const NOTIFICATION_TYPE_UNKNOWN = 0;

    /** @var int 情報 */
    const NOTIFICATION_TYPE_INFO = 1;

    /** @var int 注意 */
    const NOTIFICATION_TYPE_NOTICE = 2;

    /** @var int 警告 */
    const NOTIFICATION_TYPE_WARNING = 3;

    /** @var int エラー */
    const NOTIFICATION_TYPE_ERROR = 4;

    /** @var int 致命的エラー */
    const NOTIFICATION_TYPE_CRITICAL = 5;

    /** @var int[] 有効な通知種別リスト */
    const VALID_NOTTIFICATION_TYPES = [
        self::NOTIFICATION_TYPE_INFO,
        self::NOTIFICATION_TYPE_NOTICE,
        self::NOTIFICATION_TYPE_WARNING,
        self::NOTIFICATION_TYPE_ERROR,
        self::NOTIFICATION_TYPE_CRITICAL,
    ];

    /** @var array<int, string> 種別名リスト */
    const TYPE_NAMES = [
        self::NOTIFICATION_TYPE_UNKNOWN => 'unknown',
        self::NOTIFICATION_TYPE_INFO => 'info',
        self::NOTIFICATION_TYPE_NOTICE => 'notice',
        self::NOTIFICATION_TYPE_WARNING => 'warning',
        self::NOTIFICATION_TYPE_ERROR => 'error',
        self::NOTIFICATION_TYPE_CRITICAL => 'critical',
    ];

}
