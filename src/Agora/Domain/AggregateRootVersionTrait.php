<?php
namespace Agora\Domain;

use Cake\Chronos\Chronos;

/**
 * 集約ルートバージョントレイト
 */
trait AggregateRootVersionTrait
{

    /**
     * バージョンを生成する
     * @return string
     */
    public static function createVersion(): string
    {
        return md5(Chronos::now()->format('U.u'));
    }

}
