<?php
namespace Agora\Domain;

/**
 * 抽象ドメインサービス
 */
class DomainServiceAbstract
    implements INotificationTypes, IDomainService
{

    /** @var INotificationHandler[] 通知ハンドラリスト */
    private $notificationHandlers = [];

    /** @return INotificationHandler[] ドメイン通知ハンドラリスト */
    protected function getNotificationHandlers(): array
    {
        return $this->notificationHandlers;
    }

    /**
     * ドメイン通知ハンドラを追加する
     * @param INotificationHandler $handler ドメイン通知ハンドラ
     * @return static
     */
    public function addNotificationHandler(INotificationHandler $handler)
    {
        // 同じインスタンスがあれば追加しない
        if (!in_array($handler, $this->getNotificationHandlers(), true)) {
            $this->notificationHandlers[] = $handler;
        }
        return $this;
    }

    /**
     * ドメイン通知ハンドラリストを追加する
     * @param INotificationHandler[] $handlers ドメイン通知ハンドラリスト
     * @return static
     */
    public function addNotificationHandlers(array $handlers)
    {
        foreach ($handlers as $handler) {
            $this->addNotificationHandler($handler);
        }
        return $this;
    }

    /**
     * ドメイン通知有無リストを生成する
     * @return array
     */
    public static function createHavingTypes(): array
    {
        return array_reduce(static::VALID_NOTTIFICATION_TYPES, function ($c, $t) {
            $c[$t] = false;
            return $c;
        }, []);
    }

    /**
     * 全ての通知ハンドラに通知する
     * @param Notification $notification ドメイン通知
     * @return static
     */
    protected function notifyAll(Notification $notification)
    {
        foreach ($this->getNotificationHandlers() as $handler) {
            // @var INotificationHandler $handler
            $handler->notify($notification);
        }
        return $this;
    }

}
