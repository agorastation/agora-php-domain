<?php
namespace Agora\Domain;

/**
 * ドメイン通知
 */
class Notification
    implements IValueObject, INotificationTypes
{

    /** @var int 通知種別 */
    private $type = self::NOTIFICATION_TYPE_UNKNOWN;

    /** @var string タイトル */
    private $title = '';

    /** @var string 本文 */
    private $body = '';

    /** @var array 付属情報 */
    private $additional = [];

    /**
     * constructor
     * @param int $type 通知種別
     * @param string $title タイトル
     * @param string $body 本文
     * @param array<string, scalar> $additional OPTIONAL 付属情報
     * @return void
     */
    public function __construct(
        int $type, string $title, string $body, array $additional = [])
    {
        $type = in_array($type, self::getValidTypes())
            ? $type : self::NOTIFICATION_TYPE_UNKNOWN;
        $this->type = $type;
        $this->title = $title;
        $this->body = $body;
        $this->additional = $additional;
    }

    /**
     * ファクトリ
     * @param int $type 通知種別
     * @param string $title タイトル
     * @param string $body 本文
     * @param array<string, scalar> $additional OPTIONAL 付属情報
     * @return static
     */
    public static function create(
        int $type, string $title, string $body, array $additional = [])
    {
        return new static($type, $title, $body, $additional);
    }

    /** @static @return int[] 有効な種別リスト */
    public static function getValidTypes(): array
    {
        return [
            self::NOTIFICATION_TYPE_INFO,
            self::NOTIFICATION_TYPE_NOTICE,
            self::NOTIFICATION_TYPE_WARNING,
            self::NOTIFICATION_TYPE_ERROR,
            self::NOTIFICATION_TYPE_CRITICAL,
        ];
    }

    /** @return int 通知種別 */
    public function getType(): int
    {
        return $this->type;
    }

    /** @return string 通知種別名 */
    public function getTypeName(): string
    {
        return self::TYPE_NAMES[$this->getType()];
    }

    /** @return string タイトル */
    public function getTitle(): string
    {
        return $this->title;
    }

    /** @return string 本文 */
    public function getBody(): string
    {
        return $this->body;
    }

    /** @return array 付属情報 */
    public function getAdditional(): array
    {
        return $this->additional;
    }

}
