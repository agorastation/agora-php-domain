<?php
namespace Agora\Domain;

/**
 * 集約ルートインターフェース
 */
interface IAggregateRoot
    extends IEntity
{
}
