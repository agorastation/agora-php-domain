<?php
namespace Agora\Domain;

/**
 * 抽象値オブジェクトファクトリ
 */
class ValueObjectFactoryAbstract
{

    /** @var string クラス完全修飾名 */
    protected const CLASS_NAME = '';

    /** @var string[] プロパティ名リスト */
    protected const PROPERTIES = [];

    /** @var string クラス完全修飾名 */
    protected $className = '';

    /** @var array プロパティリスト {<property name: string>: mixed, ...} */
    protected $properties = [];

    /**
     * constructor
     * @param string $className クラス完全修飾名
     * @return void
     * @throws \InvalidArgumentException 存在しないクラス
     */
    public function __construct(string $className)
    {
        if (!class_exists($className)) {
            throw new \InvalidArgumentException("undefined class {$className}");
        }

        $this->className = $className;
        $this->properties = array_combine(
            static::PROPERTIES,
            array_fill(0, count(static::PROPERTIES), null));
    }

    /**
     * ファクトリ
     * @return static
     */
    public static function create()
    {
        return new static(static::CLASS_NAME);
    }

    /**
     * 値オブジェクトを生成する
     * @return mixed
     */
    public function build()
    {
        return \Closure::bind(function ($properties) {
            return new static(...array_values($properties));
        }, null, $this->className)->__invoke($this->properties);
    }

    /**
     * 未定義メソッドのサポート
     * @param string $name メソッド名
     * @param mixed[] $args OPTIONAL 引数リスト
     * @return static
     * @throws \BadMethodCallException 存在しないメソッド
     */
    public function __call(string $name, array $args = []): self
    {
        if (preg_match('/^set(.+)$/', $name, $matches) > 0) {
            $property = lcfirst($matches[1]);
            try {
                $this->__set($property, reset($args));
            } catch (\InvalidArgumentException $e) {
                throw new \BadMethodCallException("undefined method {$name}", -1, $e);
            }
            return $this;
        }
        throw new \BadMethodCallException("undefined method {$name}");
    }

    /**
     * 未定義フィールドへの値設定サポート
     * @param string $name フィールド名
     * @param mixed $value 値
     * @return void
     * @throws \InvalidArgumentException 存在しないフィールド
     */
    public function __set($name, $value): void
    {
        if (!array_key_exists($name, $this->properties)) {
            $class = static::class;
            throw new \InvalidArgumentException("undefined field {$class}::{$name}");
        }

        $this->properties[$name] = $value;
    }

}
