<?php
namespace Tests\Cases;

use Cake\Chronos\Chronos;

/**
 * 抽象ユニットテスト
 */
abstract class TestAbstract
    extends \PHPUnit\Framework\TestCase
{

    protected $projectRoot = PROJECT_ROOT;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();

        Chronos::setTestNow(null);
    }

}
