<?php
namespace Tests\Cases\Agora\Domain;

use Tests\Classes\Agora\Domain\TestDomainModel;

/**
 * プロパティアクセスサポート抽象ドメインモデル ユニットテスト
 * @coversDefaultClass \Agora\Domain\PropertyAccessDomainModelAbstract
 * @see \Agora\Domain\PropertyAccessDomainModelAbstract
 */
final class PropertyAccessDomainModelAbstractTest
    extends \Tests\Cases\TestAbstract
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @group propertyAccess
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_propertyAccess()
    {
        /* Arrange */
        $id = 8;
        $lastName = 'testLastName';
        $firstName = 'testFirstName';

        /* Act */
        $sut = new TestDomainModel($id, $lastName, $firstName);

        /* Assert */
        $this->assertSame($id, $sut->id);
        $this->assertSame($lastName, $sut->lastName);
        $this->assertSame($firstName, $sut->firstName);
        $this->assertSame("{$lastName} {$firstName}", $sut->fullName);

        $this->assertTrue(isset($sut->fullName));
        $this->assertFalse(empty($sut->fullName));
        $this->assertFalse(isset($sut->hoge));
        $this->assertTrue(empty($sut->hoge));
    }

    public function nonExistingDataProvider()
    {
        yield 'field' => [
            'name' => 'nonExistingField',
        ];

        yield 'getter' => [
            'name' => 'nonExistingGetter',
        ];

    }

    /**
     * @dataProvider nonExistingDataProvider
     * @group nonExisting
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_nonExisting($name)
    {
        /* Arrange */
        $id = 8;
        $lastName = 'testLastName';
        $firstName = 'testFirstName';
        $sut = new TestDomainModel($id, $lastName, $firstName);

        $className = get_class($sut);

        /* Act */
        /* Assert */
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage(
            "undefined field {$className}::{$name} for defined properties");
        $sut->{$name};
    }

    /**
     * @group undefinedProperty
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_undefinedProperty()
    {
        /* Arrange */
        $id = 8;
        $lastName = 'testLastName';
        $firstName = 'testFirstName';
        $sut = new TestDomainModel($id, $lastName, $firstName);

        $className = get_class($sut);

        /* Act */
        /* Assert */
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage(
            "undefined field {$className}::undefined");
        $sut->undefined;
    }

}
