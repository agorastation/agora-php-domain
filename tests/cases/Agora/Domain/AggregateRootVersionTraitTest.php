<?php
namespace Tests\Cases\Agora\Domain;

use Agora\Domain;
use Cake\Chronos\Chronos;

/**
 * 集約ルートバージョントレイト ユニットテスト
 * @coversDefaultClass \Agora\Domain\AggregateRootVersionTrait
 */
final class AggregateRootVersionTraitTest
    extends \Tests\Cases\TestAbstract
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @group createVersion
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_createVersion()
    {
        /* Arrange */
        Chronos::setTestNow(Chronos::parse('2019-10-25 12:34:56.654321'));
        $sut = $this->getMockForTrait(Domain\AggregateRootVersionTrait::class);

        /* Act */
        $version = $sut->createVersion();

        /* Assert */
        $this->assertSame('e7f3e60a8cced18656e9fd84cb77fcbe', $version);
    }

}
