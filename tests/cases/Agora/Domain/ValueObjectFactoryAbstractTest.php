<?php
namespace Tests\Cases\Agora\Domain;

use Agora\Domain;
use Tests\Classes\Agora\Domain\ContactFactory;

/**
 * 抽象値オブジェクトファクトリ ユニットテスト
 * @coversDefaultClass \Agora\Domain\ValueObjectFactoryAbstract
 * @see Domain\ValueObjectFactoryAbstract
 */
final class ValueObjectFactoryAbstractTest
    extends \Tests\Cases\TestAbstract
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @group factory
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_factory()
    {
        /* Arrange */
        $lastName = '姓';
        $firstName = '名';
        $area = '090';
        $city = '111';
        $branch = '2222';

        /* Act */
        $contact = ContactFactory::create()
            ->setName($lastName, $firstName)
            ->setPhoneNumber($area, $city, $branch)
            ->build();

        /* Assert */
        $this->assertSame($lastName, $contact->name->last);
        $this->assertSame($firstName, $contact->name->first);
        $this->assertSame($area, $contact->phoneNumber->area);
        $this->assertSame($city, $contact->phoneNumber->city);
        $this->assertSame($branch, $contact->phoneNumber->branch);
    }

}
