<?php
namespace Tests\Cases\Agora\Domain;

use Agora\Domain;

/**
 * ドメイン通知 ユニットテスト
 * @coversDefaultClass \Agora\Domain\Notification
 * @see Domain\Notification
 */
final class NotificationTest
    extends \Tests\Cases\TestAbstract
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function createDataProvider()
    {
        yield 'warning' => [
            'type' => Domain\Notification::NOTIFICATION_TYPE_WARNING,
            'title' => 'some.service.model.property.warning',
            'body' => 'some.service.warning.model.property.isSomething',
            'additional' => ['expect' => 3, 'actual' => 5],
            'expect' => [
                'type' => Domain\Notification::NOTIFICATION_TYPE_WARNING,
                'typeName' => 'warning',
            ],
        ];

        yield 'type out of range' => [
            'type' => 8,
            'title' => 'some.service.model.property.notype',
            'body' => 'some.service.notype.model.property.isSomething',
            'additional' => ['expect' => 3, 'actual' => 5],
            'expect' => [
                'type' => Domain\Notification::NOTIFICATION_TYPE_UNKNOWN,
                'typeName' => 'unknown',
            ],
        ];

    }

    /**
     * @dataProvider createDataProvider
     * @group create
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_create($type, $title, $body, $additional, $expect)
    {
        /* Arrange */
        /* Act */
        $notification = Domain\Notification::create($type, $title, $body, $additional);

        /* Assert */
        $this->assertSame($expect['type'], $notification->getType());
        $this->assertSame($expect['typeName'], $notification->getTypeName());
        $this->assertSame($title, $notification->getTitle());
        $this->assertSame($body, $notification->getBody());
        $this->assertSame($additional, $notification->getAdditional());
    }

}
