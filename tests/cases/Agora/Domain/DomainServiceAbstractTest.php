<?php
namespace Tests\Cases\Agora\Domain;

use Tests\Classes\Agora\Domain as TD;
use Agora\Domain;
use Prophecy\Argument;

/**
 * 抽象ドメインサービス ユニットテスト
 * @coversDefaultClass \Agora\Domain\DomainServiceAbstract
 * @see Domain\DomainServiceAbstract
 */
final class DomainServiceAbstractTest
    extends \Tests\Cases\TestAbstract
    implements Domain\INotificationTypes
{

    /** @var TD\TestDomainService */
    private $sut = null;

    public function setUp(): void
    {
        parent::setUp();

        $this->sut = new TD\TestDomainService();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @group notificationHandlers
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_notificationHandlers()
    {
        /* Arrange */
        $handlerProphecy = $this->prophesize(Domain\INotificationHandler::class);
        $handler = $handlerProphecy->reveal();
        $handlers = [
            clone $handler,
            clone $handler,
            clone $handler,
        ];

        /* Act */
        $this->sut->addNotificationHandlers($handlers);

        /* Assert */
        $this->assertSame($handlers, (function () {
            return $this->getNotificationHandlers();
        })->call($this->sut));
    }

    public function createNotificationDataProvider()
    {
        $additional = ['maxLength' => 14, 'actual' => 13];
        yield 'test.name.tooLong' => [
            'serviceName' => 'test.saving',
            'key' => 'test.name',
            'title' => 'test.name.title',
            'predicate' => 'tooLong',
            'additional' => $additional,
            'expect' => [
                'hasTypes' => [
                    self::NOTIFICATION_TYPE_ERROR => true,
                ] + Domain\DomainServiceAbstract::createHavingTypes(),
                'notification' => Domain\Notification::create(
                    self::NOTIFICATION_TYPE_ERROR,
                    'test.saving.test.name.title.error',
                    'test.saving.error.test.name.tooLong',
                    $additional),
            ],
        ];

        $additional = ['maxLength' => 14, 'actual' => 13];
        yield 'test.notModified' => [
            'serviceName' => 'test.saving',
            'key' => 'test',
            'title' => 'test.title',
            'predicate' => 'notModified',
            'additional' => [],
            'expect' => [
                'hasTypes' => [
                    self::NOTIFICATION_TYPE_WARNING => true,
                ] + Domain\DomainServiceAbstract::createHavingTypes(),
                'notification' => Domain\Notification::create(
                    self::NOTIFICATION_TYPE_WARNING,
                    'test.saving.test.title.warning',
                    'test.saving.warning.test.notModified', []),
            ],
        ];

    }

    /**
     * @group notifyAll
     * @covers ::<public>
     * @covers ::<!public>
     */
    public function test_notifyAll()
    {
        /* Arrange */
        $handlerProphecy1 = $this->prophesize(Domain\INotificationHandler::class)
            ->__call('notify', [Argument::cetera()])
            ->will(function () {return $this;})
            ->getObjectProphecy();
        $handlerProphecy2 = clone $handlerProphecy1;
        $handlers = [$handlerProphecy1->reveal(), $handlerProphecy2->reveal()];
        $this->sut->addNotificationHandlers($handlers);

        $notifications = [
            Domain\Notification::create(self::NOTIFICATION_TYPE_INFO, 'test1', 'body1'),
            Domain\Notification::create(self::NOTIFICATION_TYPE_INFO, 'test2', 'body2'),
        ];

        /* Act */
        $self = null;
        foreach ($notifications as $notification) {
            $self = (function (...$args) {
                return $this->notifyAll(...$args);
            })->call($this->sut, $notification);
        }

        /* Assert */
        $this->assertSame($this->sut, $self);

        foreach ($notifications as $notification) {
            $handlerProphecy1->__call('notify', [$notification])
                ->shouldHaveBeenCalledOnce();
            $handlerProphecy2->__call('notify', [$notification])
                ->shouldHaveBeenCalledOnce();
        }
    }

}
