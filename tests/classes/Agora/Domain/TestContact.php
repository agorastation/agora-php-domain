<?php
namespace Tests\Classes\Agora\Domain;

use Agora\Domain;

/**
 * @property-read TestName $name
 * @property-read TestPhoneNumber $phoneNumber
 */
class TestContact
    extends Domain\PropertyAccessDomainModelAbstract
    implements Domain\IValueObject
{

    /** @inheritdoc */
    protected const PROPERTIES = [
        'name' => null,
        'phoneNumber' => null,
    ];
    protected $name = null;
    protected $phoneNumber = null;

    protected function __construct(
        TestName $name, TestPhoneNumber $phoneNumber)
    {
        $this->name = $name;
        $this->phoneNumber = $phoneNumber;
    }

    public static function create(
        TestName $name, TestPhoneNumber $phoneNumber): self
    {
        return new self($name, $phoneNumber);
    }

}
