<?php
namespace Tests\Classes\Agora\Domain;

use Agora\Domain;

/**
 * @property-read string $last
 * @property-read string $first
 */
class TestName
    extends Domain\PropertyAccessDomainModelAbstract
    implements Domain\IValueObject
{
    /** @inheritdoc */
    protected const PROPERTIES = [
        'last' => null,
        'first' => null,
    ];
    protected $last = '';
    protected $first = '';

    protected function __construct(string $last, string $first)
    {
        $this->last = trim($last);
        $this->first = trim($first);
    }

    public static function create(string $last, string $first): self
    {
        return new self($last, $first);
    }

}
