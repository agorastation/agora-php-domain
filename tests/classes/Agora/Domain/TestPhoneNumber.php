<?php
namespace Tests\Classes\Agora\Domain;

use Agora\Domain;

/**
 * @property-read string $area
 * @property-read string $city
 * @property-read string $branch
 */
class TestPhoneNumber
    extends Domain\PropertyAccessDomainModelAbstract
    implements Domain\IValueObject
{

    /** @inheritdoc */
    protected const PROPERTIES = [
        'area' => null,
        'city' => null,
        'branch' => null,
    ];
    protected $area = '';
    protected $city = '';
    protected $branch = '';

    protected function __construct(
        string $area, string $city, string $branch)
    {
        $this->area = trim($area);
        $this->city = trim($city);
        $this->branch = trim($branch);
    }

    public static function create(
        string $area, string $city, string $branch): self
    {
        return new self($area, $city, $branch);
    }

}
