<?php
namespace Tests\Classes\Agora\Domain;

use Agora\Domain;

/**
 * @method TestContact build()
 */
class ContactFactory
    extends Domain\ValueObjectFactoryAbstract
{

    /** @inheritdoc */
    protected const CLASS_NAME = TestContact::class;

    /** @inheritdoc */
    protected const PROPERTIES = [
        'name',
        'phoneNumber',
    ];

    public function setName(string $last, string $first): self
    {
        return parent::setName(TestName::create($last, $first));
    }

    public function setPhoneNumber(string $area, string $city, string $branch): self
    {
        return parent::setPhoneNumber(TestPhoneNumber::create($area, $city, $branch));
    }

}
