<?php
namespace Tests\Classes\Agora\Domain;

/**
 * ドメインモデルユニットテスト向け実装
 * @property int $id ID
 * @property string $lastName 姓
 * @property string $firstName 名
 * @property string $fullName 氏名
 */
class TestDomainModel
    extends \Agora\Domain\PropertyAccessDomainModelAbstract
{

    protected const PROPERTIES = [
        'id' => null,
        'lastName' => null,
        'firstName' => null,
        'fullName' => 'getFullName',
        'nonExistingField' => null,
        'nonExistingGetter' => 'getNonExistingGetter',
    ];

    protected $id = 0;
    protected $lastName = '';
    protected $firstName = '';

    public function __construct(int $id, string $lastName, string $firstName)
    {
        $this->id = $id;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
    }

    public function getFullName(string $delimiter = ' '): string
    {
        return "{$this->lastName}{$delimiter}{$this->firstName}";
    }

}
