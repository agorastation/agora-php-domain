<?php
namespace Tests\Classes\Agora\Domain;

use Agora\Domain;

/**
 * テスト用ドメインサービス
 */
class TestDomainService
    extends Domain\DomainServiceAbstract
{

    protected const SERVICE_NAME = 'test.saving';

    protected const MESSAGES = [
        'test.saving.test' => [
            'notModified' => [self::NOTIFICATION_TYPE_WARNING],
        ],
        'test.saving.test.type' => [
            'outOfRange' => [self::NOTIFICATION_TYPE_ERROR, 'range'],
        ],
        'test.saving.test.name' => [
            'isEmpty' => [self::NOTIFICATION_TYPE_ERROR],
            'tooLong' => [self::NOTIFICATION_TYPE_ERROR, 'maxLength', 'actual'],
        ],
    ];

}
