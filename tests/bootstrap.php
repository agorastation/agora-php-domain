<?php
(function () {
    // プロジェクトルート
    $projectRoot = realpath(__dir__ . '/..') . '/';

    define('PROJECT_ROOT', $projectRoot);

    // オートローダ
    require "{$projectRoot}vendor/autoload.php";

})();
