# アゴラPHPドメインコアライブラリ

${toc}

<div style="page-break-before:always"><a href="#page-header">▲ back to index</a></div>

## モデルへのプロパティアクセス

ドメインコアライブラリの `\Agora\Domain\PropertyAccessDomainModelAbstract`を継承することで\
`オブジェクト->プロパティ名`のような記述でモデル内部の非公開フィールドにアクセス可能となる\
読み取り専用プロパティを定義することができます

例として下記のようなあるサイトの会員のモデルがあるとして

@startuml
skinparam classAttributeIconSize 0
hide empty members
left to right direction

class Member {
    + <<get>> id: int
    + <<get>> email: string
    + <<get>> dateOfBirth: Cake.Chronos.Date
    + <<get>> age: int
}
@enduml

このモデルのインスタンスを下記のように操作できます

```php
<?php
$member = new Member(...);
$mailSender->send($member->email, $subject, $body);
```

また、モデルの状態によって変化する算出プロパティの定義も可能です

```php
<?php
$member = new Member(2, 'test@mail.com', Chronos\Date::parse('1980-10-01'));
$age = $member->age; //現在日時から算出された年齢が取得される
```

モデルを定義します

```php
use Cake\Chronos;
class Member {

    /** @var int ID */
    protected $id = 0;

    /** @var string メールアドレス */
    protected $email = '';

    /** @var Chronos\Date 生年月日 */
    protected $dateOfBirth = null;

    public fuction __construct(
        int $id, string $email, Chronos\Date $dateOfBirth)
    {
        $this->id = $id;
        $this->email = $email;
        $this->dateOfBirth = $dateOfBirth;
    }

}
```

プロパティアクセスを可能とするには`\Agora\Domain\PropertyAccessDomainModelAbstract`を継承させます

```php
use Cake\Chronos;
class Member extends \Agora\Domain\PropertyAccessDomainModelAbstract {
    ...
}
```

次に公開するフィールドを定義します
```php
...
class Member extends \Agora\Domain\PropertyAccessDomainModelAbstract {
    ...
    protected const PROPERTIES = [
        'id' => null,
        'email' => null,
        'dateOfBirth' => null,
        'age' => 'getAge',
    ];
    ...
}
```

フィールドリストはキーバリュー形式で\
`プロパティ名 => 対応するgetterメソッド名`となります\
getterメソッドを介さず同名の内部フィールドを外部公開するのであれば\
getter名は`null`で定義します

次に `age`プロパティ用のgetterメソッド `getAge`を定義します

```php
<?php
...
class Member extends \Agora\Domain\PropertyAccessDomainModelAbstract {
    ...
    public fuction getAge(): int
    {
        $now = Chronos\Chronos::now();
        return $this->dateOfBirth->diffInYears($now);
    }
    ...
}
```

これによって `Member`クラスのインスタンスに対する `age`プロパティへのアクセスで
`getAge()` メソッドの返却値が取得されるようになります

> Notice:\
公開対象のフィールドの可視性は `protected`, `public` となります。\
公開フィールドリスト(`PROPERTIES`)も `protected`となります\
これは基底クラス `\Agora\Domain\PropertyAccessDomainModelAbstract`が\
リフレクションを利用せずにサブクラスのフィールドにアクセスするためです

下記はそのユニットテストとなります

```php
<?php
use Cake\Chronos;
final class PropertyAccessTest extends \PHPUnit\Framework\TestCase {

    public fuction test_properties ()
    {
        /* Arrange */
        $now = Chronos\Chronos::parse('2019-08-15');
        Chronos\Chronos::setTestNow($now);

        $id = 654;
        $email = 'test@mail.com';
        $dateOfBirth = Chronos\Date::parse('1980-10-01');

        /* Act */
        $member = new Member($id, $email, $dateOfBirth);

        /* Assert */
        $this->assertSame($last, $member->last);
        $this->assertSame($first, $member->first);
        $this->assertSame($dateOfBirth, $member->dateOfBirth);
        $this->assertSame(38, $member->age);
    }

}
```

<div style="page-break-before:always"><a href="#page-header">▲ back to index</a></div>

